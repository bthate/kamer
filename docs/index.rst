.. title:: Straffeloosheid zekerende kamerleden de cel in !!

.. image:: jpg/bewijsgif4.jpg
    :width: 100%

Geachte <naam advocaat>,

ik schrijf u om hulp te krijgen van het doen van aangifte tegen alle Eerste Kamer leden en de Koning voor het aannemen van een wet met een wettelijk voorschrift waarmee het toedienen van gif word verplicht. 
Het betreft hier het aannemen van de Wet verplichte Geestelijke Gezondheidzorg, waarvoor ik VOOR het aannemen van deze wet de Eerste Kamer heb geinformeerd dat de medicijnen die men met deze wet laat toedienen gif betreft. 
zie bijv. de brief aan de heer Aardema hieronder:

 Geachte heer Aardema,

 ik wil u graag informeren dat uw commissies van VWS en J&V geinformeerd zijn dat de medcijnen gebruikt in de gedwongen zorg waar uw wet een wettelijk voorschrift voor gaat bieden gif blijken te zijn.

 Er is bewijs dat de medcijnen gebruikt in de GGZ gif zijn, bewijs dat geleverd word door het ECHA agentschap van de Europeese Unie, wat bij houd welke chemicalien een gevaar zijn of niet. De ECHA laat bedrijven die stoffen willen transporteren testen doen wat hun dodelijkheid betreft, de zogenoemde LD-50 tests. Aan de hand van deze tests beoordeelt men dan of een stof onschadelijk, schadelijk, gif of dodelijk zijn. Deze tests resulteren in labels die men moet gebruiken als men de stof transporteert.


 Voor de stof haloperiodol, de werkzame stof in Haldol, ziet u hier de waarschuwingen die te vinden zijn op https://echa.europa.eu/substance-information/-/substanceinfo/100.000.142

 Haldol is het meest gebruikt medicijn als "nood medicatie" in de GGZ, een klassiek antipsychoticum.


 bewijs dat haldol gif is:

 .. image:: jpg/ECHAhaloperidol.png
    :width: 100%

 Zoals u ziet is haloperidol geclassificeerd als "toxic as swallowed" en is een doodskop met beenderen noodzakelijk als men deze stof wil vervoeren.

 Men gebruikt in de GGZ dus geen medicijnen die geen schade kunnen, maar een stof die giftig is als men hem inneemt.

 De medicatie die u in uw wet toestaat om gedwongen toegedient te kunnen worden zijn gif en niet zoals men beweerd een medicijn dat geen schade kan.

 Deze wet aannemen in de wetenschap dat het hier gif betreft maakt dat uw wettelijke voorschrift gebruikt word voor giftoedieningen en niet voor behandeling met medicijnen die geen schade kunnen.

 U zekert hier de straffeloosheid voor deze gif toedieningen als u niet ook zorgt dat de medicatie waar uw wet aan refereert ook niet daadwerkelijk medicijnen zijn die geen schade kunnen.

 Ik hoop dat u de wetenschap dat het hier gif betreft en niet medicatie zult gebruiken om de WvGGZ niet aan te nemen, hij word nu gebruikt in een poging gif toedieningen te legaliseren, te voorzien van een wettelijk voorschrift.


 Hoogachtend,


 Bart Thate


 p.s. de commissies VWS en J&V van uw kamer zijn ook geinformeerd maar uw persoonlijk ook op de hoogte brengen voordat men hier over stemt verdient de voorkeur.


Ik heb een bevestiging van ontvangst door de griffier van de Eerste Kamer (zie foto's hieronder).

Zou u mij kunnen helpen om aangifte te doen voor straffeloosheidzekering door de Eerste Kamer leden en de Koning ? Zou u dit kunnen doen tot  aan het EVRM als dat nodig is ? En zo ja voor welke prijs ?

Ik moet bij een voorstel eerst met mijn bewindvoerder overleggen of dat voorstel ook betaalbaar is.

Uitziend naar uw reactie, 


Bart Thate


bevestiging
===========

.. image:: jpg/bevestigd3.jpg
    :width: 100%

.. image:: jpg/bevestigd.jpg
    :width: 100%

.. toctree::
    :glob:
    :hidden:

    txt/*
    LICENSE
